import React, { Component } from 'react';
import './App.css';
import List from './components/List.js'
import Info from './components/Info.js'
import axios from 'axios'
class App extends Component {
    state = {
        countries : [],
        ourCon : [],
        borderCon :[]
    }

    getCountriesList = () => {
        const AllCountries = "all?fields=name;alpha3Code";
        axios.get(AllCountries).then(responce =>{
            let countries = responce.data;
            this.setState({countries})
            return countries;
        }).catch(error => {
            console.log(error);})
    }
    componentDidMount() {
        this.getCountriesList()
    }

    countryInfoHandler = (code) => {
        const OurCountry = "alpha/";
        axios.get(OurCountry + code).then(responce => {
            let ourCon = responce.data;
            this.setState ({ourCon})
            return responce
        }).then(responce => {
            let borders = [];

            Promise.all (responce.data.borders.map (code => {
                return axios.get(OurCountry + code).then(responce =>{
                    let borderCountry = responce.data.name;
                    borders.push(borderCountry);
                    this.setState({borderCon: borders})
                })
            }))
        }).catch(error =>{
            console.log(error);
        })
    }

    render() {
    return (
      <div className="App">
        <h1>Get Info About Countries</h1>
          <div className="listOfCountries">
              {
                  this.state.countries.map((counrty,index) => {
                      return(
                          <List key = {counrty.name} countryname = {counrty.name} countryInfoHandler = {() => this.countryInfoHandler(counrty.alpha3Code)}/>
                      )
                  })
              }
          </div>
<div className="Info-block">
    <Info
        CountryName = {this.state.ourCon.name}
        Capital = {this.state.ourCon.capital}
        PplLive = {this.state.ourCon.population}
        Flag = {this.state.ourCon.flag}
        BorderCon = {this.state.borderCon}
    >


    </Info>
</div>


      </div>
    );
  }
}

export default App;
